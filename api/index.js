const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')

const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'admin',
    password : 'admin',
    database : 'MangaStore'
})

connection.connect()

const express = require('express')
const { hash } = require('bcrypt')
const app = express()
const port = 4000

/* Middleware for Authenticating User Token */
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) {
            return res.sendStatus(403)
        }
        else {
            req.user = user
            next()
        }
    })
}

/* API for Processing User Authorization */
app.post("/login", (req, res) => {
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Customer WHERE Username='${username}'`
    connection.query( query, (err, rows) => {
        if (err) {
            res.json( {
                        "status"    :   "400",
                        "message"   :   "Error querying from store db"
            })
        }else {
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err, result) => {
                if(result){
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id" : rows[0].CustomerID,
                        "IsAdmin" : rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d' })
                    res.send(token)
                }else{
                    res.send("Invalid username / password")
                }
            })
            
        }
    })
})

/* CRUD Operation for Store Table */
app.get("/list_book", (req, res) => {
    let query = "SELECT * From Store"
    connection.query( query, (err, rows) => {
        if (err) {
            res.json( {
                        "status"    :   "400",
                        "message"   :   "Error querying from book db"
            })
        }else {
            res.json(rows)
        }
    })
})

app.post("/add_book", authenticateToken, (req, res) => {

    if (!req.user.IsAdmin) { res.send("Unauthorized Because your're not admin") }
    else {

        let book_name = req.query.book_name
        let price = req.query.price

        let query = `INSERT INTO Store 
                        (BookName, Price) 
                        VALUES ('${book_name}','${price}')`
        console.log(query)

        connection.query( query, (err, rows) => {
            if (err) {
                res.json({
                            "status"    :   "400",
                            "message"   :   "Error inserting data into db"
                })
            }else {
                res.json({
                            "status"    :   "200",
                            "message"   :   "Adding book succesful"
                })
            }
        })
    }
})

app.post("/update_book", authenticateToken, (req, res) => {

    if (!req.user.IsAdmin) { res.send("Unauthorized Because your're not admin") }
    else {

        let book_id = req.query.book_id
        let book_name = req.query.book_name
        let price = req.query.price

        let query = `UPDATE Store SET 
                        BookName='${book_name}',
                        Price='${price}'
                        WHERE BookID=${book_id}`

        console.log(query)

        connection.query( query, (err, rows) => {
            console.log(err)
            if (err) {
                res.json({
                            "status"    :   "400",
                            "message"   :   "Error updating record"
                })
            }else {
                res.json({
                            "status"    :   "200",
                            "message"   :   "Updating book succesful"
                })
            }
        })
    }
})

app.post("/delete_book", authenticateToken, (req, res) => {

    if (!req.user.IsAdmin) { res.send("Unauthorized Because your're not admin") }
    else {

        let book_id = req.query.book_id

        let query = `DELETE FROM Store
                        WHERE BookID=${book_id}`

        console.log(query)

        connection.query( query, (err, rows) => {
            console.log(err)
            if (err) {
                res.json({
                            "status"    :   "400",
                            "message"   :   "Error deleting record"
                })
            }else {
                res.json({
                            "status"    :   "200",
                            "message"   :   "Deleting record success"
                })
            }
        })
    }
})

/* API for Registering a new User */
app.post("/register", (req, res) => {
    let name = req.query.name
    let surname = req.query.surname
    let username = req.query.username
    let password = req.query.password

    bcrypt.hash(password, SALT_ROUNDS, (err, hash) => {

        let query = `INSERT INTO Customer 
                    (Name, SurName, Username, Password, IsAdmin) 
                    VALUES ('${name}','${surname}','${username}','${hash}', false)`
        console.log(query)

        connection.query( query, (err, rows) => {
            if (err) {
                res.json({
                            "status"    :   "400",
                            "message"   :   "Error inserting data into db"
                })
            }else {
                res.json({
                            "status"    :   "200",
                            "message"   :   "Adding new user succesful"
                })
            }
        })
    })
})

/* API for buy the book */
app.post("/buy_book", authenticateToken, (req, res) => {

    let customer_id = req.user.user_id
    
    let book_id = req.query.book_id
    let quantity = req.query.quantity

    let query = `INSERT INTO BuyOrder 
                    (CustomerID, BookID, Quantity, OrderTime) 
                    VALUES ('${customer_id}','${book_id}',${quantity},NOW() )`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status"    :   "400",
                        "message"   :   "Error inserting data into db"
            })
        }else {
            res.json({
                        "status"    :   "200",
                        "message"   :   "Adding book succesful"
            })
        }
    })
})

/* List All Order */
app.post("/all_order", authenticateToken, (req, res) => {

    if (!req.user.IsAdmin) { res.send("Unauthorized Because your're not admin") }
    else {

        let query = `
        SELECT  Customer.CustomerID, Customer.Name, Customer.SurName, Store.BookName, 
                Store.Price, BuyOrder.OrderTime 
                FROM Customer, Store, BuyOrder
                WHERE (BuyOrder.CustomerID = Customer.CustomerID) AND 
                (BuyOrder.BookID = Store.BookID)`
        
        connection.query( query, (err, rows) => {
            if (err) {
                res.json( {
                            "status"    :   "400",
                            "message"   :   "Error querying from store db"
                })
            }else {
                res.json(rows)
            }
        })
    }
})

/* List All Order by a User ID */
app.post("/list_order", authenticateToken, (req, res) => {
    
    let customer_id = req.user.user_id

    let query = `
    SELECT Customer.CustomerID, Store.BookID, Store.BookName, 
        Store.Price, BuyOrder.Quantity, BuyOrder.OrderTime 
        FROM Customer, Store, BuyOrder 
        WHERE (Customer.CustomerID = BuyOrder.CustomerID) AND 
        (BuyOrder.BookID = Store.BookID) AND 
        (Customer.CustomerID = ${customer_id})`
        
    connection.query( query, (err, rows) => {
        if (err) {
            res.json( {
                        "status"    :   "400",
                        "message"   :   "Error querying from store db"
            })
        }else {
            res.json(rows)
        }
    })
})

app.listen(port, () => {
    console.log(`Now starting Manga Store System Backend ${port}`)
})