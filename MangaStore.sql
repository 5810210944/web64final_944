-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 03, 2022 at 07:02 PM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `MangaStore`
--

-- --------------------------------------------------------

--
-- Table structure for table `BuyOrder`
--

CREATE TABLE `BuyOrder` (
  `OrderID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `BookID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `OrderTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `BuyOrder`
--

INSERT INTO `BuyOrder` (`OrderID`, `CustomerID`, `BookID`, `Quantity`, `OrderTime`) VALUES
(1, 2, 2, 1, '2022-04-03 18:29:20'),
(2, 2, 1, 1, '2022-04-03 18:29:20'),
(3, 3, 2, 1, '2022-04-03 18:47:15'),
(4, 3, 3, 1, '2022-04-03 18:47:40');

-- --------------------------------------------------------

--
-- Table structure for table `Customer`
--

CREATE TABLE `Customer` (
  `CustomerID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `SurName` varchar(50) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `IsAdmin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Customer`
--

INSERT INTO `Customer` (`CustomerID`, `Name`, `SurName`, `Username`, `Password`, `IsAdmin`) VALUES
(2, 'Pekora', 'Usada', 'pekora', '123456', 0),
(3, 'Miko', 'Sakura', 'miko', '$2b$10$pxfTLanvEsy1cnv0eI.3WOwkqtCYv/1BapKsAdcyoEgtR8Bshe6Y2', 0),
(4, 'Admin', 'Admin', 'admin', '$2b$10$5iauiyDroROzngqV9rGcR.J.j9grcOy7YTxtkwoke5qcBbXlYIL8W', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Store`
--

CREATE TABLE `Store` (
  `BookID` int(11) NOT NULL,
  `BookName` varchar(100) NOT NULL,
  `Price` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Store`
--

INSERT INTO `Store` (`BookID`, `BookName`, `Price`) VALUES
(1, 'เส้นทางพลิกผันของราชันอมตะ', 85),
(2, 'เกิดชาตินี้พี่ต้องเทพ', 85),
(3, 'สารภาพรักกับคุณคางุยะซะดีๆ', 70),
(4, 'ชิโอมิจัง ยากูซ่าขาลุย', 60),
(5, 'โฉมงามพูดไม่เก่งกับผองเพื่อนไม่เต็มเต็ง', 70);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `BuyOrder`
--
ALTER TABLE `BuyOrder`
  ADD PRIMARY KEY (`OrderID`),
  ADD KEY `CustomerID` (`CustomerID`),
  ADD KEY `BookID` (`BookID`);

--
-- Indexes for table `Customer`
--
ALTER TABLE `Customer`
  ADD PRIMARY KEY (`CustomerID`);

--
-- Indexes for table `Store`
--
ALTER TABLE `Store`
  ADD PRIMARY KEY (`BookID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `BuyOrder`
--
ALTER TABLE `BuyOrder`
  MODIFY `OrderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Customer`
--
ALTER TABLE `Customer`
  MODIFY `CustomerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `Store`
--
ALTER TABLE `Store`
  MODIFY `BookID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `BuyOrder`
--
ALTER TABLE `BuyOrder`
  ADD CONSTRAINT `BookID` FOREIGN KEY (`BookID`) REFERENCES `Store` (`BookID`),
  ADD CONSTRAINT `CustomerID` FOREIGN KEY (`CustomerID`) REFERENCES `Customer` (`CustomerID`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
